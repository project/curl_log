### How to use

Replace curl_exec with curl_log_exec in your code.

This will automatically log all curl requests that take longer than 'curl_log_duration' to the 'curl_log' database table.

### Advanced usage

There is a 2nd and 3rd parameter to ensure that strings are sanitized:

  $post_data = array('foo' => 'bar');
  $result = curl_log_exec($ch, $post_data, array_values($post_data));

This will replace every occurence of 'bar' with 'XXX' in the logged data.

Warning: You need to ensure that no integers that could interfere with the
         serialization data are replaced.

This can be used to securely log credit card and customer data without leaking
sensitive data to the 'curl_log' logging table.
